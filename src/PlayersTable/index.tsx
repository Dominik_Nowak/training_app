import React, { useState } from 'react'
import { Player } from '../types'
import players from '../players.json'
import { AddPlayer } from '../AddPlayer';

export const PlayersTable: React.FC = () => {
    const [playersList, setPlayersList] = useState<Player[]>(players);
    const [highlightingColor, setHighlightingColor] = useState("red");

    const tableOfPoints = playersList.map((player => player.points));

    const totalPoints = tableOfPoints.reduce((pV, cV) => {
        return pV + cV;
    });

    const average = (totalPoints / tableOfPoints.length).toFixed(2);

    const handleClick = (color: string) => {
        setHighlightingColor(color);
    };

    const handleDeletePlayer = (id: number) => {
        const newPlayerList = playersList.filter((value) => { return value.id !== id });
        setPlayersList(newPlayerList);
    }

    const handleAddPlayer = (newPlayers: Player[]) => {
        setPlayersList(newPlayers);
    }

    return (
        <>
            <button onClick={() => handleClick("red")}>Red</button>
            <button onClick={() => handleClick("blue")}>Blue</button>
            <table>
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Points</th>
                    </tr>
                </thead>
                <tbody>
                    {playersList.map((player: Player) => <tr key={player.id} style={player.points > 100 ?
                        { color: 'white', backgroundColor: highlightingColor } :
                        { color: 'black', backgroundColor: 'white' }}>
                        <td>{player.username}</td>
                        <td>{player.points}</td>
                        <td><button onClick={() => handleDeletePlayer(player.id)}>Delete</button></td>
                    </tr>
                    )}
                </tbody>
                <tfoot>
                    <tr>
                        <th>
                            Total:
                    </th>
                        <td>{totalPoints}</td>
                    </tr>
                    <tr>
                        <th>
                            Average:
                    </th>
                        <td>{average}</td>
                    </tr>
                </tfoot>
            </table>
            <AddPlayer players={playersList} onPlayersListChange={handleAddPlayer} />
        </>
    )
};