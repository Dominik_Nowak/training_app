import React, { useState } from 'react'
import { Player } from '../types'

interface AddPlayerProps {
    players: Player[];
    onPlayersListChange(players: Player[]): void;
}

export const AddPlayer: React.FC<AddPlayerProps> = (props) => {
    const [inputUsername, setInputUsername] = useState("");

    const randomNumber = (min: number, max: number): number => {
        return Math.floor(Math.random() * (max - min) + min);
    }

    const checkId = (): number => {
        try {
            return props.players[props.players.length - 1].id + 1;
        } catch (e) {
            console.log(e.message);
            return 0;
        }
    }

    const addPlayer = (): Player[] => {
        let newPlayer = {
            id: checkId(),
            username: inputUsername,
            points: randomNumber(0, 150)
        }
        return [...props.players, newPlayer];
    }

    const handleClick = () => {
        props.onPlayersListChange(addPlayer());
    }

    const inputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInputUsername(e.currentTarget.value);
    }

    return (
        <>
            <input id='inputUsername' value={inputUsername} onChange={inputChange}></input>
            <button onClick={handleClick}>Add player</button>
        </>
    )
}
