import React from 'react';
import './App.css';
import { PlayersTable } from './PlayersTable';


export const App: React.FC = () => {
  return (
    <div className="App">
      <PlayersTable />
    </div>
  );
}

