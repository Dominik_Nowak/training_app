export type Player = {
    id: number;
    username: string;
    points: number;
}